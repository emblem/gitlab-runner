#!/bin/bash
set -x

BASE_DIR=$(pwd)
PERSISTENT=$BASE_DIR/.persistent
SRC_DIR=gitlab-runner-src
SCRIPT_NAME=$0

# backup this script, create PERSISTENT if not exist
cat "$SCRIPT_NAME" > "$SCRIPT_NAME.backup" 
#if [ ! -d "$PERSISTENT" ]; then mkdir -p "$PERSISTENT"

# update sources
rm -rf "$SRC_DIR"
git clone https://gitlab.com/emblem/gitlab-runner "$SRC_DIR"
cd "$SRC_DIR"

# config
echo "PERSISTENT=$PERSISTENT" > .env
# pull
docker compose pull gitlab-runner
# down
docker compose down --remove-orphans
# up
docker compose up -d gitlab-runner

# update self
cd "$BASE_DIR"
cp "$SRC_DIR/$SCRIPT_NAME" $BASE_DIR/run-script.sh -f
chmod +x "$SCRIPT_NAME"
  
