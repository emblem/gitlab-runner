#!/bin/bash
PERSISTENT=/persistent/gitlab-runner
docker run --rm -it -v "$PERSISTENT":/etc/gitlab-runner gitlab/gitlab-runner register
