#!/bin/bash
PERSISTENT=/persistent/gitlab-runner
SRC_DIR=gitlab-runner-src
SCRIPT_NAME=$0
cd "$PERSISTENT"

# update sources
rm -rf "$SRC_DIR"
git clone https://gitlab.com/emblem/gitlab-runner "$SRC_DIR"
cd "$SRC_DIR"

# config
echo "PERSISTENT=$PERSISTENT" > .env
# pull
docker compose pull gitlab-runner
# down
docker compose down --remove-orphans
# up
docker compose up -d gitlab-runner

# update self
cd "$PERSISTENT"
cp "$SRC_DIR/$SCRIPT_NAME" . -f
chmod +x "$SCRIPT_NAME"
